package com.trial;

import com.trial.Account;
import com.trial.AccountRepository;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class WebController {
    private AccountRepository accountRepository;

    public WebController(AccountRepository accountRepository){
        this.accountRepository = accountRepository;
    }

    @RequestMapping("/hello")
    public String hello(){
        return "Hello World";
    }

    @GetMapping("/accounts")
    public List<Account> getAccounts(){
        List<Account> accounts = this.accountRepository.findAll();
        return accounts;
    }
}

