package com.trial;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;


@Entity
public class Account {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private long id;
    private String name;
    private int accountType;
    private boolean isOpen;

    protected Account(){}

    public Account(String name, int accountType, boolean isOpen){
        this.name = name;
        this.accountType = accountType;
        this.isOpen = isOpen;
    }

    public long getId(){
        return id;
    }

    public String getName() {
        return name;
    }

    public int getAccountType() {
        return accountType;
    }

    public boolean isOpen() {
        return isOpen;
    }

}
