package com.trial;

import com.trial.Account;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.ArrayList;

@Component
public class DbSeeder implements CommandLineRunner {
    private AccountRepository accountRepository;
    public DbSeeder(AccountRepository accountRepository){
        this.accountRepository = accountRepository;
    }

    @Override
    public void run(String... strings) throws Exception{
        Account tommy = new Account("Tommy", 1,true);
        Account noel = new Account("Noel", 1,true);
        Account howard = new Account("Howard", 1,true);

        List<Account> accounts = new ArrayList<Account>();
        accounts.add(tommy);
        accounts.add(noel);
        accounts.add(howard);

        this.accountRepository.saveAll(accounts);
    }
}
